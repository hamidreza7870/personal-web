import { Grid, Typography } from '@material-ui/core'
import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import "../components/portfolio.css";

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: 30,
    position: "relative",
    width: "100%",
    paddingRight: 15,
    paddingLeft: 15,
    cursor: "pointer"
  },
  portfolioVisible: {
    visibility: "visible",
    opacity: 1,
    transition: "all .4s ease-out",
  },
  image: {
    width: "100%",
    verticalAlign: "middle",
  },
  titlePortfolio: {
    transition: "4s",
    "&:hover": {
      color: theme.palette.primary.main,
      transition: "4s",
    }
  },
  descPortfolio: {
    color: "#a4acc4",
    marginBottom: 0,
    fontWeight: 500,

  },
}));

export default function Portfolio({ image }) {
  const classes = useStyles();
  return (
    <>
      <Grid item md={4} sm={6} className={classes.root}>
        <Grid className={classes.portfolioVisible}>
          <Grid className="portfolioImage" >
            <img src={image} alt="imagePortfolio" className={classes.image} />
          </Grid>
          <Typography variant="h5" className={classes.titlePortfolio} >JavaScript project</Typography>
          <Typography variant="h6" className={classes.descPortfolio} >first personal project in season of learn</Typography>
        </Grid>
      </Grid>
    </>
  )
}
