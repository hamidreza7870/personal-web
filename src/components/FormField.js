import React from 'react'
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    labelStyle: {
        position: "absolute",
        left: 15,
        top: -9,
        fontWeight : "400",
        background: "#10121b",
        transition: "all .4s ease-out",
        pointerEvents: "none",
        fontSize: ".94rem",
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 10,
        paddingRight: 10,
    },
    formField: {
        marginTop: 30,
        position: "relative",
    },
    inputStyle: {
        border: "1px solid #2e344e",
        fontSize: "1rem",
        height: 50,
        width: "100%",
        // fontSize: 14,
        verticalAlign: "middle",
        background: "transparent",
        color: "#a4acc4",
        overflow: "visible",
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 15,
        paddingRight: 15,
    },
  }));

export default function FormField({label }) {
    const classes = useStyles();
    return (
        <div className={classes.formField}>
            <label for="contact-form-name" className={classes.labelStyle}>{label}</label>
            <input type="text" name="name" className={classes.inputStyle} ></input>
        </div>
    )
}
