import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import { Grid , Typography } from '@material-ui/core';
const useStyles = makeStyles(theme => ({
    root: {
        marginTop: 20,
        paddingLeft : 15,
        paddingRight: 15,
        textAlign : "left"
    },
    main: {
        border: "1px solid #2e344e",
        borderTop: "5px solid #2e344e",
        background: "#191d2b",
        transition: ".4s",
        padding: theme.spacing(8),
        "&:hover": {
            borderTopColor: theme.palette.primary.main,
            transition : ".4s"
        }
    },
    title: {
        fontWeight: 600,
        position: "relative",
        paddingBottom: 15,
        marginBottom: 15,
        "&::before": {
            content: "''",
            position: "absolute",
            left: 0,
            top: "auto",
            bottom: 0,
            height: 2,
            width: 50,
            background: "#2e344e",
        }
    },
  }));

export default function ServiceCart({title , desc , icon}) {
    const classes = useStyles();
    return (
    <>
        <Grid item lg={4} md={6} xs={12} className={classes.root}>
            <Grid container className={classes.main} direction="column" alignItems="flex-start" >
                {icon}
                <Typography variant="h5" className={classes.title} >{title}</Typography>
                <Typography variant="body1" className={classes.desc} >{ desc}</Typography>
            </Grid>
       </Grid>
    </>
    )
}
