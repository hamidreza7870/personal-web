import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    contactInfoblock: {
        marginTop: 30,
        background: "#191d2b",
        padding: 30,
        display: "flex",
    },
    contactInfoblockIcon: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: 70,
        width: 70,
        maxWidth: 70,
        border: "1px solid #2e344e",
        marginRight: 20,
    },
    infoTitle: {
        marginTop: -5,
    },
  }));

export default function ContactInfo({icon, title, detail1, detail2 }) {
    const classes = useStyles();
    return (
        <div className={classes.contactInfoblock}>
            <span className={classes.contactInfoblockIcon}>
                {icon}
            </span>
            <Grid className={classes.contactInfoblockContent}>
                <Typography variant="h6" className={classes.infoTitle} >{title}</Typography>
                <Typography variant="body1" className={classes.infodesc} >{detail1}</Typography>
                <Typography variant="body1" className={classes.infodesc} >{detail2}</Typography>
            </Grid>
        </div>
    )
}

