import React from 'react';
import { Grid, makeStyles, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    fadeTitle : {
        position: "absolute",
        left: 0,
        top: "100%",
        lineHeight: 1,
        fontWeight: 900,
        color: "rgba(25,29,43,.44)",
        zIndex: -1,
        transform: "translateY(-40%)",
        userSelect: "none",
        whiteSpace: "nowrap",
        fontSize: "4.5rem",
    },
    sectionTitle: {
        marginBottom: 60,
        position: "relative",
        zIndex: 1,
    },
    aboutTitle: {
        [theme.breakpoints.down("md")]: {
            fontSize: "2.37rem",
            lineHeight: "3.0857rem",
        },
        [theme.breakpoints.down("xs")]: {
            fontSize: "2.17rem",
            lineHeight: "2.857rem",
        },
        [theme.breakpoints.up("md")]: {
            fontSize: "2.57rem",
            lineHeight: "3.2857rem",
        },
        display: "flex",
        paddingBottom : 15,
        "&::after": {
            textAlign: "left",
            content: "''",
            position: "absolute",
            left: 0,
            top: "auto",
            bottom: 0,
            height: 5,
            borderRadius: 100,
            width: 35,
            background: "#037fff",
        },
        "&::before": {
            content: "''",
            position: "absolute",
            left: 0,
            top: "auto",
            bottom: 0,
            height: 5,
            borderRadius: 100,
            width: 100,
            background: "rgba(3,127,255,.3)",
        }
    }
}))
export default function Title({title}) {
  const classes = useStyles();
    return (
        <Grid className={classes.sectionTitle} >
            <Typography variant="h2" className={classes.aboutTitle} > { title } </Typography>
            <span className={classes.fadeTitle}> { title } </span>
        </Grid>
    )
}
