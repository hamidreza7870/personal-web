import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography } from '@material-ui/core';
import { getTranslate } from "../localization/index"
import Title from './Title';
import aboutImage from "../asets/images/aboutImage.jpg";

const useStyles = makeStyles(theme => ({
    about: {
        // [theme.breakpoints.down("xl")]: {
        //     maxWidth: 1140,
        // },
        // [theme.breakpoints.down("lg")]: {
        //     maxWidth: 1140,
        //     margin:"0 15px",
        // },
        // [theme.breakpoints.down("md")]: {
        //     maxWidth: 720,
        //     margin:"auto",
        // },
        // // [theme.breakpoints.down("sm")]: {
        // //     maxWidth : 540,
        // // },
        // paddingTop: 120,
    },
    container: {
        [theme.breakpoints.down("md")]: {
            maxWidth : 1140,
        },
        [theme.breakpoints.down("sm")]: {
            maxWidth : 720,
        },
        marginLeft : "auto",
        marginRight: "auto",
        paddingRight:15,
        paddingLeft:15,
        paddingTop:120
    },
    aboutImage: {
        position: "relative",
        "&::before": {
            content: "''",
            position: "absolute",
            height: "65%",
            width: 15,
            background: "rgba(3,127,255,.6)",
        },
        "&::after": {
            content: "''",
            position: "absolute",
            height: "65%",
            width: 15,
            background: "rgba(3,127,255,.6)",
            left: "auto",
            right: 0,
            top: "auto",
            bottom: 0,
        }
    },
    mainImage :{
        width: "100%",
        height: "100%",
    },
    nameText: {
        color: theme.palette.primary.main,
        [theme.breakpoints.down('xs')]: {
            display: "block",
            fontSize: "3.2rem"
        },
    },
    name: {
        [theme.breakpoints.down('sm')]: {
            fontSize: "1.91rem",
            lineHeight: "2.8rem",
            width:"100%"
        }
    },
    aboutContent: {
        [theme.breakpoints.down('sm')]: {
            paddingLeft: 0,
            marginTop : 30,
        },
        position: "relative",
        width: "100%",
        paddingLeft : 30,
        paddingRight : 15,
    },
    desc: {
        marginBottom : "1rem"
    },
    ulDesc: {
        marginTop: 0,
        marginBottom : "1rem",
        padding: 0,
        display: "inline-block",
        listStyle : "none",
    },
    downloadButtom: {
        position: "relative",
        paddingTop: 0,
        paddingBottom: 0,
        paddingRight : 30 ,
        paddingLeft : 30 ,
        background: "#037fff",
        color: "#fff",
        border: 0,
        display: "inline-block",
        zIndex: 1,
        textTransform: "uppercase",
        fontSize: "0.9rem",
        letterSpacing: 2,
        height: 50,
        lineHeight: "3.125rem",
        textDecoration : "none",
        transition: "all .4s ease-out",
        "&:hover": {
            "&::befor": {
                content: "''",
                position: "absolute",
                left: 0,
                top: "auto",
                bottom: 0,
                height: 2,
                width: "100%",
                background: "#fff",
                zIndex: -1,
                // transform: scaleX(0),
                transition: "all .4s ease-out",
            }
        }
    },
    btnDownloadResume: {
        textAlign: "left",
        width:"100%"
    },
    aboutWrapper: {
        marginTop:30,
    }
  }));

export default function AboutMe() {
    const classes = useStyles();
    const translate = getTranslate();
    return (
        <Grid container className={classes.root}>
             <Grid container className={classes.about}>
                <Grid container className={classes.container}>
                    <Title title={ translate.aboutMe }/>
                    <Grid container className={classes.row}>
                        <Grid item sm={12} md={6} lg={6} className={classes.aboutImage}>
                            <img alt="aboutImage" src={aboutImage} className={classes.mainImage} />
                        </Grid>
                        <Grid container item xs={12} md={6} className={classes.aboutContent}>
                            <Grid container className={classes.aboutWrapper}>
                                <Typography className={classes.name} variant="h3"> {translate.hi} <span className={classes.nameText}> {translate.name} </span><span>{translate.hi2}</span>
                                </Typography>
                                <Typography variant="body1" className={classes.desc}> {translate.desc} </Typography>
                                <Typography variant="body1" >
                                    <ul className={classes.ulDesc}>
                                        <li style={{marginBottom:3}}>
                                            <b style={{ display: "inline-block", minWidth: 130 }}>{ translate.fullname }</b>
                                        : {translate.name}
                                        </li>
                                        <li style={{marginBottom:3}}>
                                            <b style={{display : "inline-block" , minWidth : 130}}>{translate.age}</b>
                                        : 27 {translate.years}
                                        </li>
                                        <li style={{marginBottom:3}}>
                                            <b style={{display : "inline-block" , minWidth : 130}}>{translate.nationality}</b>
                                        : {translate.iranian}
                                        </li>
                                        <li style={{marginBottom:3}}>
                                            <b style={{display : "inline-block" , minWidth : 130}}>{translate.languages}</b>
                                        : {translate.persian} , {translate.english}
                                        </li>
                                        <li style={{marginBottom:3}}>
                                            <b style={{display : "inline-block" , minWidth : 130}}>{translate.address}</b>
                                        : {translate.iran} , {translate.tehran} 
                                        </li>
                                        <li style={{marginBottom:3}}>
                                            <b style={{display : "inline-block" , minWidth : 130}}>{translate.phone}</b>
                                        : (+98)9369590062
                                        </li>
                                    </ul>
                                </Typography>
                                <div className={classes.btnDownloadResume} >
                                    <a href="/" className={classes.downloadButtom}>
                                        {translate.downloadcv}
                                    </a>
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}
