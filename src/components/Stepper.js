import { Grid, Typography } from '@material-ui/core'
import React from 'react'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    borderLeft: "3px solid #2e344e",
  },
  resumeWrapper: {
    display: "flex",
    marginTop: 30
  },
  resumeSummary: {
    alignSelf: "flex-start",
    display: "flex",
    flexGrow: 0,
    flexShrink: 0,
    flexBasis: 220,
    maxWidth: 220,
    paddingLeft: 20,
    position: "relative",
    [theme.breakpoints.down('sm')]: {
      display: "none"
    },
  },
  resumeSummaryHidden: {
    [theme.breakpoints.down('sm')]: {
      display: "flex"
    },
    alignSelf: "flex-start",
    display: "none",
    flexGrow: 0,
    flexShrink: 0,
    flexBasis: 220,
    maxWidth: 220,
    paddingLeft: 20,
    position: "relative",
  },
  resumeYear: {
    color: "#a4acc4",
    marginBottom: 0,
    "&::before": {
      content: "''",
      position: "absolute",
      left: -9,
      top: 6,
      height: 15,
      width: 15,
      borderRadius: 100,
      background: "#10121b",
      border: "3px solid #2e344e",
    },
  },
  resumeYearHidden: {
    [theme.breakpoints.down('sm')]: {
      display: "block"
    },
    display: "none",
    color: "#a4acc4",
    marginBottom: 0,
    "&::before": {
      content: "''",
      position: "absolute",
      left: -9,
      top: 6,
      height: 15,
      width: 15,
      borderRadius: 100,
      background: "#10121b",
      border: "3px solid #2e344e",
    },
  },
  resumeDetails: {
    "&::before": {
      content: "''",
      position: "absolute",
      left: 0,
      top: 15,
      height: 1,
      width: 30,
      background: "#2e344e",
    },
    position: "relative",
    paddingLeft: 50,
  },
  resumeTitle: {
    color: "#037fff",
    marginBottom: 0,
    fontweight: 700,
  },
}));

export default function Stepper({ title, company, desc, year, }) {
  const classes = useStyles();
  return (
    <Grid container className={classes.root}>
      <Grid className={classes.resumeWrapper}>
        <Grid className={classes.resumeSummary}>
          <Typography variant="h6" className={classes.resumeYear} >{year}</Typography>
        </Grid>
        <Grid className={classes.resumeDetails}>
          <Grid className={classes.resumeSummaryHidden}>
            <Typography variant="h6" className={classes.resumeYearHidden} >{year}</Typography>
          </Grid>
          <Typography variant="h5" className={classes.resumeTitle}>{title}</Typography>
          <Typography variant="h6" className={classes.resumeCompany}>{company}</Typography>
          <Typography variant="body1">{desc}</Typography>
        </Grid>
      </Grid>
    </Grid>
  )
}
