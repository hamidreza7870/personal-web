import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography } from '@material-ui/core';
import { getTranslate } from "../localization/index"
import Title from './Title';
import { BusinessCenter , LocalLibrary } from '@material-ui/icons';
import Stepper from './Stepper';

const useStyles = makeStyles(theme => ({
    container: {
        [theme.breakpoints.down("lg")]: {
            maxWidth : 1140,
        },
        [theme.breakpoints.down("md")]: {
            maxWidth : 1140,
        },
        [theme.breakpoints.down("sm")]: {
            maxWidth : 720,
        },
        [theme.breakpoints.down("xs")]: {
            maxWidth : 540,
        },
        marginLeft : "auto",
        marginRight: "auto",
        paddingRight:15,
        paddingLeft:15,
        paddingTop:120
    },
    iconWork: {
        verticalAlign: "middle",
        marginRight: 10,
        fontSize: "1.71rem",
        lineHeight: "2.43rem",
        color: "rgb(164, 172, 196)",
    },
    resumeListUp: {
        marginBottom: 30,
        marginTop :30,
    },
    resumeListBottom: {
        marginBottom: 30,
        marginTop: 30,
        paddingBottom : 120
    },
  }));

export default function AboutMe() {
    const classes = useStyles();
    const translate = getTranslate();
    return (
        <Grid container className={classes.root}>
             <Grid container className={classes.resume}>
                <Grid className={classes.container}>
                    <Title title={ translate.resume }/>
                    <Grid container direction="row" justify="flex-start" alignItems="center" className={classes.smalTitle} >
                        <BusinessCenter className={ classes.iconWork }/>
                        <Typography variant="h4" >{translate.mainTitleResume1}</Typography>
                    </Grid>
                    <Grid container className={classes.resumeListUp}>
                        <Stepper title={translate.resumeTitle1} company={translate.company1} desc={translate.resumeDesc} year={translate.year1} />
                        <Stepper title={translate.resumeTitle1} company={translate.company2} desc={translate.resumeDesc} year={translate.year2} />
                        <Stepper title={translate.resumeTitle2} company={translate.company3} desc={translate.resumeDesc} year={translate.year3} />
                    </Grid>
                    <Grid container direction="row" justify="flex-start" alignItems="center" className={classes.smalTitle} >
                        <LocalLibrary className={ classes.iconWork }/>
                        <Typography variant="h4" >{translate.mainTitleResume2}</Typography>
                    </Grid>
                    <Grid container className={classes.resumeListBottom}>
                        <Stepper title={translate.resumeTitle3} company={translate.University1} desc={translate.resumeDesc} year={translate.year1} />
                        <Stepper title={translate.resumeTitle4} company={translate.University2} desc={translate.resumeDesc} year={translate.year2} />
                        <Stepper title={translate.resumeTitle5} company={translate.University3} desc={translate.resumeDesc} year={translate.year3} />
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}
