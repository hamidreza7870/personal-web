import React from 'react'
import { Typography, List, ListItem, ListItemText, Button } from '@material-ui/core'
import ProfileImage from '../asets/images/profileImage.jpg'
import { getTranslate, lang } from '../localization/index'
import { Link, useLocation } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles';

const drawerWidth = 260;
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('md')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
    [theme.breakpoints.down('md')]: {
      width: drawerWidth,
      flexShrink: 0,
      display: "none"
    },
  },
  bottomDrawer: {
    padding: 15,
    borderTop: "1px solid #2e344e",
  },
  topDrawer: {
    width: "100%",
    padding: 20,
    borderBottom: "1px solid #2e344e",
    textAlign: "center"
  },
  profileImage: {
    width: 186,
    height: 186,
    maxWidth: "100%",
    borderRadius: 100,
    border: "7px solid #2e334e",
    verticalAlign: "middle"
  },
  drawerPaper: {
    width: drawerWidth,
    height: "100vh",
    borderRight: "1px solid #2e334e",
    backgroundColor: "#191d2b"
  },
  buttonDrawer: {
    width: "100%",
    textAlign: "center",
    padding: 15,
    borderTop: "1px solid #2e344e"
  },
  centerDrawer: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  content: {
    flexGrow: 1,
    paddingRight: theme.spacing(3),
    paddingLeft: theme.spacing(3),
    position: "relative",
    height: '100%'
  },
}));

const basePath = lang === "fa" ? "/fa" : ""
export default function Menu({ setMobileOpen }) {
  const classes = useStyles();
  const translate = getTranslate()
  const location = useLocation().pathname
  let changeLangPath = lang === "fa" ? location.substring(3) : "/fa" + location
  const chekActiveRoute = (path) => {
    if (location === path || location === `/fa${path}` || location === `/fa${path}/` || (path === "/" && location === "/fa")) {
      return true
    }
    return false
  }
  return (
    <>
      <div className={classes.topDrawer}>
        <img src={ProfileImage} alt={translate.name} className={classes.profileImage} />
      </div>
      <div className={classes.centerDrawer}>
        <List style={{ width: "100%" }} >
          <ListItem
            className={chekActiveRoute("/") ? null : "listItem"}
            style={{ backgroundColor: chekActiveRoute("/") ? "#037fff" : "transparent", paddingLeft: 0, paddingRight: 0 }}
            onClick={() => { setMobileOpen(false) }}
            component={Link}
            to={`${basePath}/`}
            button>
            <ListItemText
              disableTypography={true}
              children={<Typography className="listItemText"
                style={{ color: chekActiveRoute("/") ? "#fff" : "#a4acc4" }}
                variant="body2" >{translate.home}</Typography>}
            />
            <div className="overlay" />
          </ListItem>
          <ListItem
            className={chekActiveRoute("/about") ? null : "listItem"}
            style={{ backgroundColor: chekActiveRoute("/about") ? "#037fff" : "transparent", paddingLeft: 0, paddingRight: 0 }}
            onClick={() => { setMobileOpen(false) }}
            component={Link}
            to={`${basePath}/about`}
            button>
            <ListItemText
              disableTypography={true}
              children={<Typography className="listItemText"
                style={{ color: chekActiveRoute("/about") ? "#fff" : "#a4acc4" }}
                variant="body2" >{translate.about}</Typography>}
            />
            <div className="overlay" />
          </ListItem>
          <ListItem
            className={chekActiveRoute("/resume") ? null : "listItem"}
            style={{ backgroundColor: chekActiveRoute("/resume") ? "#037fff" : "transparent", paddingLeft: 0, paddingRight: 0 }}
            onClick={() => { setMobileOpen(false) }}
            component={Link}
            to={`${basePath}/resume`}
            button>
            <ListItemText
              disableTypography={true}
              children={<Typography className="listItemText"
                style={{ color: chekActiveRoute("/resume") ? "#fff" : "#a4acc4" }}
                variant="body2" >{translate.resume}</Typography>}
            />
            <div className="overlay" />
          </ListItem>
          <ListItem
            className={chekActiveRoute("/portfolio") ? null : "listItem"}
            style={{ backgroundColor: chekActiveRoute("/portfolio") ? "#037fff" : "transparent", paddingLeft: 0, paddingRight: 0 }}
            onClick={() => { setMobileOpen(false) }}
            component={Link}
            to={`${basePath}/portfolio`}
            button>
            <ListItemText
              disableTypography={true}
              children={<Typography className="listItemText"
                style={{ color: chekActiveRoute("/portfolio") ? "#fff" : "#a4acc4" }}
                variant="body2" >{translate.portfolios}</Typography>}
            />
            <div className="overlay" />
          </ListItem>
          <ListItem
            className={chekActiveRoute("/contact") ? null : "listItem"}
            style={{ backgroundColor: chekActiveRoute("/contact") ? "#037fff" : "transparent", paddingLeft: 0, paddingRight: 0 }}
            onClick={() => { setMobileOpen(false) }}
            component={Link}
            to={`${basePath}/contact`}
            button>
            <ListItemText
              disableTypography={true}
              children={<Typography className="listItemText"
                style={{ color: chekActiveRoute("/contact") ? "#fff" : "#a4acc4" }}
                variant="body2" >{translate.contact}</Typography>}
            />
            <div className="overlay" />
          </ListItem>
        </List>
      </div>
      <div className={classes.bottomDrawer}>
        <Button color={lang === "fa" ? "primary" : "secondary"}
          component="a" href={changeLangPath}
        >فارسی</Button>
        {'/'}
        <Button color={lang === "en" ? "primary" : "secondary"}
          component="a" href={changeLangPath}
        >English</Button>
      </div>
    </>
  )
}
