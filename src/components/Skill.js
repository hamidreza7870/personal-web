import React , {useEffect, useState} from 'react'
import { makeStyles } from "@material-ui/core/styles";
import { Grid, LinearProgress, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop : 30,
    },
    row: {
        width: "100%",
        marginRight: 30,  
    },
    progres: {
        display: "flex",
        alignItems: "center",
    },
    progresPercentag: {
        display : "flex",
        flexGrow: 0,
        flexShrink: 0,
        flexBasis: 60,
    },
    progresLinear: {
        width: "100%",
        height: 6,
        background:"#2e344e",
        
    }
  }));

export default function Skill({value , title}) {
    const [val, setVal] = useState(0);
    const classes = useStyles();
    useEffect(() => {
        setTimeout(() => {
            if (val === value) {
                return
            }
            setVal(val => val+5)
        }, 60);
    }, [val, value])
    // useEffect(() => {
    //     setTimeout(() => {setVal(value)}, 500);
    // }, [])
    return (
        <Grid container className={classes.root}>
            <Grid className={classes.row}>
                <Typography variant="h6" >{title}</Typography>
                <div className={classes.progres}>
                    <Typography variant="body1" className={classes.progresPercentag}>{value}%</Typography>
                    <LinearProgress className={classes.progresLinear} value={val} variant="determinate" color="primary" ></LinearProgress>
                </div>
            </Grid>
        </Grid>
    )
}
