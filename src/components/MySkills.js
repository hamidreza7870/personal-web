import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import { Grid } from '@material-ui/core';
import { getTranslate } from "../localization/index"
import Title from './Title';
import Skill from './Skill';

const useStyles = makeStyles(theme => ({
    root: {
    },
    skills: {
        paddingTop: 120,
    },
    container: {
        [theme.breakpoints.down("md")]: {
            maxWidth : 1140,
        },
        [theme.breakpoints.down("sm")]: {
            maxWidth : 720,
        },
        marginLeft : "auto",
        marginRight: "auto",
        paddingRight:15,
        paddingLeft: 15,
        width:"100%"
    },
    row: {
        marginTop : -30
    }
  }));

export default function AboutMe() {
    const classes = useStyles();
    const translate = getTranslate();
    return (
        <Grid container className={classes.root}>
             <Grid container className={classes.skills}>
                <Grid className={classes.container}>
                    <Title title={ translate.myskills }/>
                    <Grid container className={classes.row}>
                        <Grid container item xs={12} md={6} >
                            <Skill value={95} title={"HTML"} />
                            <Skill value={80} title={"CSS3"} />
                            <Skill value={70} title={"JavaScript"} />
                        </Grid>
                        <Grid container item xs={12} md={6} >
                            <Skill value={95} title={"ReactJS"} />
                            <Skill value={90} title={"Bootstrap"} />
                            <Skill value={75} title={"Photoshop"} />
                        </Grid>
                    </Grid>
                </Grid>

            </Grid>
        </Grid>
    )
}
