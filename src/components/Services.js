import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import { Grid} from '@material-ui/core';
import { getTranslate } from "../localization/index"
import Title from './Title';
import ServiceCart from './serviceCart';
import { Palette , Code , PhoneAndroid} from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
    container: {
        [theme.breakpoints.down("md")]: {
            maxWidth : 1140,
        },
        [theme.breakpoints.down("sm")]: {
            maxWidth : 720,
        },
        marginLeft : "auto",
        marginRight: "auto",
        paddingRight:15,
        paddingLeft:15,
    },
    services: {
        margin:"auto",
        paddingTop: 120,
    },
    itemService: {
        marginBottom : 60,
        marginLeft: -15,
        marginRight: -15,
        display: "flex",
        flexDirection : "row",
        [theme.breakpoints.down('sm')]: {
            flexDirection : "column"
        }
    },
    iconStyle: {
        fontSize: 46,
        color: theme.palette.primary.main,
        marginBottom : 20
    },
  }));

export default function Services() {
    const classes = useStyles();
    const translate = getTranslate();
    return (
    <Grid container className={classes.root}>
        <Grid container className={classes.services}>
            <Grid container item xs={12}  className={classes.container}>
                    <Title title={translate.services} />
                    <Grid container className={classes.itemService}>
                        <ServiceCart icon={<Palette      className={classes.iconStyle} />} title={translate.WebDesign} desc={translate.serviceDesc} />
                        <ServiceCart icon={<Code         className={classes.iconStyle} />} title={translate.WebDevelopment} desc={translate.serviceDesc} />
                        <ServiceCart icon={<PhoneAndroid className={classes.iconStyle} />} title={translate.MobileApplication} desc={translate.serviceDesc} />
                    </Grid>
            </Grid>
        </Grid>
    </Grid>
    )
}

