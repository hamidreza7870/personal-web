import React from "react"
import { BrowserRouter } from "react-router-dom"
import them from "./them/theme"
import { ThemeProvider, StylesProvider, jssPreset } from "@material-ui/core/styles"
import { CssBaseline } from "@material-ui/core"
import { create } from "jss"
import rtl from "jss-rtl"
import { getDirection } from "./localization/index"
import IndexScrean from "./screens/index"

const jss = create({ plugins: [...jssPreset().plugins, rtl()] })
function App() {
  return getDirection() === "ltr" ? (
    <BrowserRouter>
      <ThemeProvider theme={them}>
        <StylesProvider >
          <CssBaseline />
          <IndexScrean />
        </StylesProvider>
      </ThemeProvider>
    </BrowserRouter>
  ) : (
      <BrowserRouter>
        <ThemeProvider theme={them}>
          <StylesProvider jss={jss}>
            <CssBaseline />
            <IndexScrean />
          </StylesProvider>
        </ThemeProvider>
      </BrowserRouter>
    )
}
export default App;