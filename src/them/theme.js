import { createMuiTheme } from '@material-ui/core/styles'
import palette from './palette'
import {getDirection , getFont } from "../localization/index"
let theme = createMuiTheme()
theme = createMuiTheme({
  direction: getDirection(),
  palette,
  spacing: 2.5,
  typography: {
    h1: {
      fontWeight : 700,
      fontSize: getDirection() === "rtl" ? 50 : 52,
      lineHeight: "4rem",
      fontFamily: getFont(),
      color: "#fff",
      // textAlign : "left"
    },
    h2: {
      fontWeight : 700,
      fontSize: getDirection() === "rtl" ? 38 : 40,
      lineHeight: "3.2857rem",
      fontFamily: getFont(),
      color: "#fff",
      textAlign : "left"
    },
    h3: {
      fontWeight: 600,
      fontSize: getDirection() === "rtl" ? 30 : 32,
      lineHeight: "2.7rem",
      fontFamily: getFont(),
      color: "#fff",
      textAlign : "left"
    },
    h4: {
      fontWeight : 700,
      fontSize: getDirection() === "rtl" ? 26 : 28,
      lineHeight: "2.43rem",
      fontFamily: getFont(),
      color: "#fff",
      textAlign : "left"
    },
    h5: {
      fontWeight : 600,
      fontSize: getDirection() === "rtl" ? 21 : 23,
      lineHeight: "2.14rem",
      fontFamily : getFont(),
      color: "#fff",
      textAlign : "left"
    },
    h6: {
      fontWeight : 600,
      fontSize: getDirection() === "rtl" ? 16 : 18,
      lineHeight: "1.857rem",
      fontFamily : getFont(),
      color: "#fff",
      textAlign : "left"
    },
    body1: {
      fontWeight : 400,
      fontSize: getDirection() === "rtl" ? 16 : 18,
      lineHeight: "1.8rem",
      fontFamily: getFont(),
      color: "#a4acc4",
      textAlign : getDirection() === "rtl"? "justify" : "left"
    },
    body2:{
      letterSpacing : 1,
      fontSize: getDirection() === "rtl" ? 13 : 15,
      padding: 5,
      color: "#a4acc4",
      fontWeight: 600,
      textAlign : "center",
      fontFamily : getFont()
    },
    subtitle1: {
      fontWeight : 400,
      fontSize: getDirection() === "rtl" ? 18 : 20,
      lineHeight: "2rem",
      fontFamily : getFont()
    },
    button: {
      fontWeight : 400,
      fontSize: getDirection() === "rtl" ? 12 : 14,
      color: "#fff",
      letterSpacing : 2,
      fontFamily : getFont()
    }
  }
})
export default theme
