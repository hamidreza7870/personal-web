import en_us from "./en"
import fa_ir from "./fa"

let regex = /^\/fa/i
const lang = regex.test(window.location.pathname) ? "fa" : "en"
export {lang}
const directions = {
    fa: "rtl",
    en: "ltr"
}
function getDirection() {
    return directions[lang]
}
export { getDirection }

const translates = {
    fa: fa_ir,
    en: en_us
}

function getTranslate() {
    return translates[lang]
}
export { getTranslate }

const fonts = {
    fa: "IRANSans",
    en: "Nunito"
}
function getFont() {
    return fonts[lang]
}

function changeLang(newLang) {
    if (newLang === lang) {
        return
    }
    localStorage.lang = newLang
    window.location.reload()
}
export { changeLang }


export { getFont }

