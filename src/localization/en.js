// eslint-disable-next-line import/no-anonymous-default-export
export default {
    name: "Hamidreza Jahandideh",
    home: "HOME",
    about: "ABOUT",
    resume: "RESUME",
    portfolios: "PORTFOLIOS",
    contact: "CONTACT ME",
    hi: "Hi, I am ",
    hi2: "",
    desc : "One year I started learning programming and during this time I worked as a tutor and I have a few examples of work. I hope I can make more progress .",
    aboutMe: "ABOUT ME",
    services: "SERVICES",
    fullname: "Full Name",
    age:"Age",
    nationality: "Nationality",
    languages : "Languages",
    address: "Address",
    phone: "Phone",
    years: "Years",
    iranian: "Iranian",
    persian: "Persian",
    english: "English",
    tehran: "Tehran",
    iran: "Iran",
    downloadcv: "DOWNLOAD CV",
    WebDesign: "Web Design",
    WebDevelopment: "Web Development,",
    MobileApplication: "Mobile Application",
    serviceDesc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem tenetur ratione quod.",
    myskills: "MY SKILLS",
    resumeDesc: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quas, magni mollitia, aspernatur consequatur accusamus vero eum facere exercitationem velit suscipit ipsam placeat libero. Deleniti exercitationem nostrum quasi. Molestiae, vel porro.",
    mainTitleResume1: "Working Experience",
    mainTitleResume2: "Educational Qualifications",
    resumeTitle1: "Frontend Web Developer",
    resumeTitle2: "UI/UX Designer",
    resumeTitle3: "Master of Science",
    resumeTitle4: "Bachelor of Science",
    resumeTitle5: "Higher Schoold Graduation",
    year1 : "2018 - 2019",
    year2 : "2016 - 2018",
    year3: "2014 - 1016",
    company1 : "Abc Company",
    company2 : "CBA Company",
    company3 : "Example Company",
    University1 : "Abc University",
    University2 : "cba University",
    University3: "example University",
    contactTitle : "Get In Touch",
    nameLabel : "Enter your name*",
    emailLabel : "Enter your email*",
    subjectLabel : "Enter your subject*",
    messageLabel: "Enter your Message*",
    buttonContact: "SEND MAIL",
    email: "Email",
    location : "Tehran - Iran "
}

 