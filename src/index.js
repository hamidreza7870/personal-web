import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import "./asets/fonts/IRANSans/WebFonts/css/fontiran.css";
import { getDirection } from './localization/index';
import reportWebVitals from './reportWebVitals';

document.getElementsByTagName("body")[0].setAttribute("dir" , getDirection())
ReactDOM.render(<App /> , document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
