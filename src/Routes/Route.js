import React, { useEffect } from 'react'
import { Switch, Route, useLocation } from 'react-router-dom'
import Home from "../screens/Home";
import About from "../screens/About";
import ResumePage from "../screens/ResumePage";
import Portfolios from "../screens/Portfolios";
import Contact from "../screens/Contact";
import NotFound from '../components/NotFound';

export default function Index() {
  let location = useLocation().pathname
  useEffect(() => {
    window.scrollTo(0, 0)
  }, [location])
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/fa">
        <Home />
      </Route>
      <Route exact path="/about">
        <About />
      </Route>
      <Route exact path="/fa/about">
        <About />
      </Route>
      <Route exact path="/contact">
        <Contact />
      </Route>
      <Route exact path="/fa/contact">
        <Contact />
      </Route>
      <Route exact path="/resume">
        <ResumePage />
      </Route>
      <Route exact path="/fa/resume">
        <ResumePage />
      </Route>
      <Route exact path="/portfolio">
        <Portfolios />
      </Route>
      <Route exact path="/fa/portfolio">
        <Portfolios />
      </Route>
      <Route path="*">
        <NotFound />
      </Route>
    </Switch>
  )
}
