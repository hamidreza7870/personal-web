import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import { getTranslate } from "../localization/index";
import "./HomeEffect.css"
import { Instagram, Telegram, Twitter } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
    root: {
        minHeight: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        zIndex: -5,
        overflow: "hidden",
    },
    nameText: {
        color: theme.palette.primary.main,
        [theme.breakpoints.down('xs')]: {
            display: "block",
            fontSize: "3.2rem"
        }
    },
    homeText: {
        marginLeft: "auto",
        marginRight: "auto",
        padding: "0 15px 0 15px",
        zIndex : 2,
    },
    divIcon: {
        marginTop: 25,
        
    },
    IconButtonStyle: {
        outline:"none",
        color:"#a4acc4",
        backgroundColor: "transparent",
        border: "2px solid #2e344e",
        height: 45,
        width: 45,
        lineHeight: "45px",
        textAlign: "center",
        borderRadius: "100px",
        fontSize: "1.3rem",
        marginRight: 8,
        marginLeft: 8,
        transition : ".5s",
        "&:hover" :{
            borderColor: theme.palette.primary.main,
            color : theme.palette.primary.main,
            transition : ".5s"
        }
    },
    backgroundEffect: {
        zIndex: -1,
        minHeight: "100vh",
        
    },
    desc: {
        marginBottom: 16,
        marginTop : 16
    }
}));
  
export default function Home() {
  const classes = useStyles();
    const translate = getTranslate()
    return (
        <div className={classes.root}>
            <div className={classes.backgroundEffect}>
                <div id="stars"></div>
                <div id="stars2"></div>
                <div id="stars3"></div>
            </div>
            <div className={classes.homeText}>
                <Typography variant="h1">{translate.hi} <span className={classes.nameText}>{ translate.name } </span><span>{translate.hi2}</span></Typography>
                <Typography variant="subtitle1" className={classes.desc}> {translate.desc} </Typography>
                <div className={classes.divIcon}>
                    <button type="submit" aria-label="telegram" className={classes.IconButtonStyle}>
                        <Telegram />
                    </button>
                    <button type="submit" aria-label="twitter" className={classes.IconButtonStyle}>
                        <Twitter />
                    </button>
                    <button type="submit" aria-label="instagram" className={classes.IconButtonStyle}>
                        <Instagram />
                    </button>
               </div>
            </div>
        </div>
    )
}
