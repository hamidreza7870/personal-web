import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import { Grid } from '@material-ui/core';
import { getTranslate } from "../localization/index"
import Title from '../components/Title';
import Portfolio from "../components/Portfolio";
import image1 from "../asets/images/js.jpeg";
import image2 from "../asets/images/html5.png";
import image3 from "../asets/images/css3.jpg";
import image4 from "../asets/images/reactjs.png";

const useStyles = makeStyles(theme => ({
    root: {
        minHeight: "100vh",
    },
    portfolios: {
        paddingTop: 120,
    },
    container: {
        [theme.breakpoints.down("md")]: {
            maxWidth : 1140,
        },
        [theme.breakpoints.down("sm")]: {
            maxWidth : 720,
        },
        marginLeft : "auto",
        marginRight: "auto",
        paddingRight:15,
        paddingLeft:15,
    },
}));

export default function Portfolios() {
    const classes = useStyles();
    const translate = getTranslate();
    return (
        <Grid container className={classes.root}>
            <Grid container className={classes.portfolios}>
                <Grid item container direction="row" justify="flex-start" alignItems="center" xs={12} className={classes.container}>
                    <Title title={translate.portfolios} />
                    <Grid container className={classes.row}>
                        <Portfolio image={image1}/>
                        <Portfolio image={image2}/>
                        <Portfolio image={image3}/>
                        <Portfolio image={image4}/>
                        <Portfolio image={image1}/>
                        <Portfolio image={image2}/>
                        <Portfolio image={image3}/>
                        <Portfolio image={image4}/>
                        <Portfolio image={image1}/>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}
