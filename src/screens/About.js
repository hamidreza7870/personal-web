import React from 'react';
import { makeStyles } from "@material-ui/core/styles";
import { Grid } from '@material-ui/core';
import AboutMe from "../components/AboutMe";
import Services from '../components/Services';

const useStyles = makeStyles(theme => ({
    root: {
        minHeight: "100vh",
    },
  }));

export default function About() {
    const classes = useStyles();
    return (
        <Grid container className={classes.root}>
                <AboutMe />
                <Services />
        </Grid>
    )
}
