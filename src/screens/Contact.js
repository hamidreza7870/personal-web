import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography } from '@material-ui/core';
import { getTranslate } from "../localization/index"
import Title from '../components/Title';
import FormField from '../components/FormField';
import ContactInfo from '../components/ContactInfo';
import PhoneIcon from '@material-ui/icons/Phone';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import LocationOnIcon from '@material-ui/icons/LocationOn';

const useStyles = makeStyles(theme => ({
    contact: {
        paddingTop: 120,
    },
    container: {
        [theme.breakpoints.up("md")]: {
            minWidth: 1140,
        },
        [theme.breakpoints.down("md")]: {
            maxWidth: 1140,
        },
        [theme.breakpoints.down("sm")]: {
            maxWidth: 540,
        },
        // [theme.breakpoints.down("xs")]: {
        //     maxWidth: 540,
        // },
        marginLeft: "auto",
        marginRight: "auto",
        paddingRight: 15,
        paddingLeft: 15,
    },
    infoContact: {
        marginBottom: 120,
    },
    formWrapper: {
        marginBottom: 120,
    },
    titleContact: {
        marginBottom: ".5rem",
        fontWeight: 500,
    },
    textareaStyle: {
        border: "1px solid #2e344e",
        fontSize: "1rem",
        paddingTop: 10,
        paddingRight: 15,
        paddingBottom: 10,
        paddingLeft: 15,
        width: "100%",
        background: "transparent",
        color: "#a4acc4",
    },
    labelStyle: {
        position: "absolute",
        left: 15,
        top: -13,
        background: "#10121b",
        transition: "all .4s ease-out",
        pointerEvents: "none",
        fontSize: ".94rem",
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 10,
        paddingRight: 10,
    },
    formField: {
        marginTop: 30,
        position: "relative",
        textAlign: "left"
    },
    buttonStyle: {
        outline: "none",
        position: "relative",
        background: "#037fff",
        color: "#fff",
        border: 0,
        display: "inline-block",
        zIndex: 1,
        textTransform: "uppercase",
        fontSize: ".9rem",
        letterSpacing: 2,
        height: 50,
        transition: ".4s",
        paddingTop: 0,
        paddingRight: 30,
        paddingBottom: 0,
        paddingLeft: 30,
        cursor: "pointer",
        fontFamily: "IRANSans",
        "&:hover": {
            "&::before": {
                content: "''",
                position: "absolute",
                left: 0,
                top: "auto",
                bottom: 0,
                height: 2,
                width: "100%",
                background: "#fff",
                zIndex: -2,
                transition: ".4s",
            }
        }
    },
    contactInfo: {
        marginTop: -30,
        position: "relative",
        width: "100%",
        paddingRight: 15,
        paddingLeft: 30,
    }
}));

export default function Contact() {
    const classes = useStyles();
    const translate = getTranslate();
    return (
        <Grid container className={classes.root}>
            <Grid container className={classes.contact}>
                <Grid className={classes.container}>
                    <Title title={translate.contact} />
                    <Grid container className={classes.row}>
                        <Grid item xs={12} md={6} className={classes.formContact}>
                            <Grid className={classes.formWrapper}>
                                <Typography variant="h4" className={classes.titleContact} >
                                    {translate.contactTitle}
                                </Typography>
                                <FormField label={translate.nameLabel} />
                                <FormField label={translate.emailLabel} />
                                <FormField label={translate.subjectLabel} />
                                <div className={classes.formField}>
                                    <label for="contact-form-name" className={classes.labelStyle}>{translate.messageLabel}</label>
                                    <textarea cols="30" rows="6" type="text" name="message" className={classes.textareaStyle} ></textarea>
                                </div>
                                <div className={classes.formField}>
                                    <button className={classes.buttonStyle}>{translate.buttonContact}</button>
                                </div>
                            </Grid>
                        </Grid>
                        <Grid item xs={12} md={6} className={classes.infoContact}>
                            <Grid className={classes.contactInfo} >
                                <ContactInfo icon={<PhoneIcon style={{ fontSize: 40 }} />} title={translate.phone} detail1="+98-9369590062" detail2="+012-3456-7891" />
                                <ContactInfo icon={<MailOutlineIcon style={{ fontSize: 40 }} />} title={translate.email} detail1="hammidreza7870@yahoo.com" detail2="info.sitename@example.com" />
                                <ContactInfo icon={<LocationOnIcon style={{ fontSize: 40 }} />} title="Address" detail1={translate.location} detail2="" />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}