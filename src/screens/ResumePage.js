import React from 'react';
import { makeStyles } from "@material-ui/core/styles";
import { Grid } from '@material-ui/core';
import MySkills from "../components/MySkills"
import Resume from '../components/Resume';

const useStyles = makeStyles(theme => ({
  }));

export default function ResumePage() {
    const classes = useStyles();
    return (
        <Grid container className={classes.root}>
            <MySkills />
            <Resume />
        </Grid>
    )
}
