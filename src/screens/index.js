import React, { useState } from 'react';
import { Drawer, CssBaseline, IconButton, Hidden } from '@material-ui/core'
import Menu from "../components/Menu"
import Route from "../Routes/Route"
import MenuIcon from '@material-ui/icons/Menu';
import "./index.css"
import { makeStyles } from '@material-ui/core/styles';

const drawerWidth = 260;
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('md')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
    [theme.breakpoints.down('md')]: {
      width: drawerWidth,
      flexShrink: 0,
      display: "none"
    },
  },
  menuButton: {
    position: "absolute",
    top: 20,
    textAlign: "center",
    background: theme.palette.primary.backgroundColor,
    border: "1px solid #2e344e",
    borderRadius: 0,
    zIndex: 5,
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
    [theme.breakpoints.down('md')]: {
      display: 'flex',
      position: "fixed"
    },
  },
  drawerPaper: {
    width: drawerWidth,
    height: "100vh",
    borderRight: "1px solid #2e334e",
    backgroundColor: "#191d2b"
  },
  content: {
    flexGrow: 1,
    paddingRight: theme.spacing(3),
    paddingLeft: theme.spacing(3),
    position: "relative",
    height: '100%'
  },
}));

function ResponsiveDrawer(props) {
  const classes = useStyles();
  const { window } = props;
  const [mobileOpen, setMobileOpen] = useState(false);
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const container = window !== undefined ? () => window().document.body : undefined;
  return (
    <div className={classes.root}>
      <CssBaseline />
      <IconButton
        color="inherit"
        aria-label="open drawer"
        edge="start"
        onClick={handleDrawerToggle}
        className={classes.menuButton}
      >
        <MenuIcon />
      </IconButton>
      <nav className={classes.drawer} aria-label="mailbox folders">
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            <Menu classes={classes} setMobileOpen={setMobileOpen} />
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            <Menu classes={classes} setMobileOpen={setMobileOpen} />
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <span className="line1"></span>
        <span className="line2"></span>
        <span className="line3"></span>
        <span className="line4"></span>
        {/* {getPage()} */}
        <Route />
      </main>
    </div>
  );
}


export default ResponsiveDrawer;